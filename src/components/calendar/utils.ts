function limitDay(day: number) {
    if (day < 0) { day = 0; }
    return day;
}

function limitHour(hour: number, duration: number) {
    if (hour < 0) { hour = 0; }
    if (hour > 24 - duration) { hour = 24 - duration; }
    return hour;
}

function checkDuration(duration: number): boolean {
    if (duration < 1 || duration > 24) { return false; }
    return true;
}

interface IRect {
    left: number;
    right: number;
    top: number;
    bottom: number;
}

function intersectRect(rectA: IRect, rectB: IRect): boolean {
    return !(
        rectB.left >= rectA.right ||
        rectB.right <= rectA.left ||
        rectB.top >= rectA.bottom ||
        rectB.bottom <= rectA.top
    );
}

export {limitDay, limitHour, checkDuration, intersectRect};
